#!/bin/sh

#========================
#
#========================
find_git_repos() {
find $1 -not -path "*.repo*" -name .git | cut -d/ -f 2
}

#========================
#
#========================
checkout_git_branch() {
pushd .; cd $1; echo $1; git checkout $2; git pull; popd
}

#========================
#
#========================
gc_checkcout_usage () {
 # checkout master Branch
 echo "To checkout master Branch -- Replace '$0'"	
 echo "find_git_repos . | xargs -n 1 -P 10 bash -c '. gcmds.sh; checkout_git_branch "$0" master'"
}

# find_git_repos . | xargs -n 1 -P 10 bash -c '. gcmds.sh; checkout_git_branch "$0" master'

#========================
#
#git checkout -b develop; git push --set-upstream SWDP develop
# $1 is path
# $2 is upstream origin or SWDP
# $3 is branch name
#========================
gc_branch_setupstream () {
pushd .; cd $1; echo $1; git checkout -b $3; git push --set-upstream $2 $3; popd
}

#========================
#
# $1 is path
# $2 is upstream to remote: origin or SWDP
#========================
gc_branch_develop () {
	gc_branch_setupstream $1 $2 develop
}


#========================
# hardcoded SWDP, TODO export remote
#========================
gc_branch_develop_all () {
	find_git_repos . | xargs -n 1 -P 1 bash -c '. gcmds.sh; gc_branch_develop "$0" SWDP'
}

#========================
#
#========================
